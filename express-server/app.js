const { json } = require('express');
const  cors  = require('cors');
const express = require('express');
// se require body-parser para acceder a los datos del body
const bodyParser = require('body-parser')
var app = express();
//app.use(express.json);
app.use(cors());
const port = 3000;
app.listen( port, () => console.log(`server is runing in port: ${port}`));
app.use(bodyParser.json());

var Ciudades = ["Mendoza", "Neuquen", "San Luis"];
var miDestinos = ["Mendoza", "Cordoba"];

app.get('/ciudades', (req, res) => {
  console.log('in get /ciudades , headers: ' + JSON.stringify(req.headers));
  res.status(200);
  if(req.query.q){
    console.log('whit req.query.q: ' + req.query.q + "\n");
    res.send(JSON.stringify(Ciudades.filter( (c) => 
    c.toLocaleLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1 )));
  } else {
    console.log(' get all \n')
    res.json(miDestinos);
  }
});

app.post('/ciudades/create', (req, res) => {
  console.log(req.body);
  miDestinos.push(req.body.nuevo);
  console.log('in post, body.nuevo: ' + req.body.nuevo);
  res.json(miDestinos);
});

app.get('/api/translation', (req, res, next) => {
  console.log('--> value lang: ' + req.query.lang);
  res.json({
    lang:req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang
  });
});