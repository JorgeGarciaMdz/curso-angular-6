import express = require('express');
import { Destinos } from './destino.controller';
import { Destino } from './destino.model';

export let router = express.Router();

let destinos = new Destinos([]);
let Ciudades: string[] = [];
let query: string = '';

router.get('', (req, res ) => {
  console.log('/ciudades its work!')
  // res.send(destinos.getAll());
  Ciudades = [];
  destinos.getAll().forEach(value => 
    Ciudades.push(value.getNombre()));
  console.log('Destinos: ' + JSON.stringify(Ciudades));
  if(!req.query.q){
    res.send([]);
  } else {
    query = req.query.q.toString();
    console.log('req.query.q.toString(): ' + query);
    console.log('--> ' + JSON.stringify(Ciudades.filter(c => c.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) > -1 )));
    /*res.send(JSON.stringify(dest.filter( f => 
      f.toLowerCase().indexOf(query.toString().toLowerCase()) > -1))); */
    res.json( Ciudades.filter(c => c.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) > -1 ));
  }
});

router.post('/create', (req, res) => {
  destinos.add(new Destino( destinos.getNewId(), req.body.nuevo));
  console.log('objeto agregado, Ciudad: ' + JSON.stringify(destinos));
  res.send(destinos);
});

router.get('/:id', ( req, res ) => {
  console.log('id: ' + req.params.id);
  res.send(destinos.getById( Number.parseInt(req.params.id, 10)));
});
