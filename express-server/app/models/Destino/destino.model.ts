export interface IDestino {
  _id?: number;
  _nombre?: string;
}

export class Destino implements IDestino {
  constructor( public _id: number, public _nombre: string) {
    this._id = 0;
  }
  
  setId(id: number): void {
    this._id = id;
  }
  getId(): number {
    return this._id;
  }
  getNombre(): string {
    return this._nombre;
  }

  
  public get nombre(): string  {
    return this._nombre;
  }
  
  public set nombre(nombre : string) {
    this._nombre= nombre;
  }
  
  
}

