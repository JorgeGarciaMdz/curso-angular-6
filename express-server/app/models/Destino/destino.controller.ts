import { Destino } from './destino.model';

export class Destinos {
  private destinos: Destino[];
  constructor( destinos: Destino[]){
    this.destinos = [];
  }

  add(destino: Destino): void {
    const i: number = this.destinos.length + 1;
    destino.setId(i);
    this.destinos.push(destino)
    console.log(' Destino agregado --> Destinos: ' + this.destinos);
  }

  getAll(): Destino[] {
    return this.destinos;
  }

  getById(id: number): Destino {
    return this.destinos.filter( destino => {
      return destino.getId() == id;
    })[0];
  }

  getNewId(): number {
    return ( this.destinos.length + 1);
  }
}