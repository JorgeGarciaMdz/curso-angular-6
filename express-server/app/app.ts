// app/app.ts
import express = require('express');
import bodyParser = require('body-parser');
import cors = require('cors');

// importamos routers
import { router as routerDestino} from './models/Destino/destino.route' ;

// create a new express application instance
const app: express.Application = express();
const port: number = 3000;

app.use(bodyParser.json());
app.use(express.json());
app.use(cors());

app.use('/ciudades', routerDestino);

app.use('/api/translation', (req, res, next) => {
  console.log('--> value lang: ' + req.query.lang);
  res.json({
    lang:req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang
  });
});

app.listen( port, () => {
  console.log(`App listening in port ${port}`);
});