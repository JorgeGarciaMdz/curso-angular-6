# README  #

### En directorio "express-server" ###
## SERVIDOR EXPRESS en JS ##
1. instalar dependencias
  npm install

2. levantar servidor
  npm start

## SERVIDOR EXPRESS con TYPESCRIPT ##

1. instalar typescript
  npm install -g typescript

2. instalar dependencias
  npm install

3. levantar servidor
  npm run dev