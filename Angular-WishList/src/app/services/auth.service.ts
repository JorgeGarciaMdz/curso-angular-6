import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn(): boolean {
    return this.getUser() !== null;
  }
  logout(): any {
    localStorage.removeItem('username');
  }
  login(username: string, password: string): boolean {
    if (username === 'user' && password === 'password') {
      localStorage.setItem('username', username);
    }
    return false;
  }

  getUser(): any {
    return localStorage.getItem('username');
  }

  constructor() { }
}
