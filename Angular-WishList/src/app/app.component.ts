import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular-WishList';
  time = new Observable( observer => {
    setInterval(() => observer.next( new Date().toString()), 1000 ); // es un observador de string
    console.log('el tiempo se bindea con el template del componente');
  });

  // agregando constructor para usar translate
  constructor( public translate: TranslateService) {
    console.log('---> In constructor of AppComponent, call getTranslation');
    translate.getTranslation('en').subscribe( x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }
}
