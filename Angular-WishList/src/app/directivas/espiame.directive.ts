import { Directive, OnInit, OnDestroy } from '@angular/core';

// onInit and OnDistroy son los ciclos de vida de la directiva
/**
 * Evento OnInit es una iterfaz con un metodo a implementar
 */

 // declaramos la directiva que luego la agregamos en app.module -> declarations, lugo en listado.destino.html
@Directive({
  selector: '[appEspiame]'  // se vincula a componentes html que quieren ser espiados
})
export class EspiameDirective implements OnInit, OnDestroy {
  static nextId = 0;

  constructor() { }

  log = (msg: string) => console.log(`--> Evento #${++EspiameDirective.nextId} ${msg}`);
  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
    this.log('###> OnInit');
  }
  ngOnInit(): void {
    // throw new Error('Method not implemented.');
    this.log('###< OnDestroy');
  }

}
