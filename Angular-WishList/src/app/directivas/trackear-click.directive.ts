import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private elementRef: HTMLInputElement;

  /*
  por inyeccion de dependencia recibimos el elementos sobre el cual estamos aplicando la directiva
  en este ejemplo, recibimos el componente sobre el cual se aplica la directiva [appTrackearClick]
  Basicamente podemos acceder a los elementos del dom en el cual se aplica la directiva.
  Lo vamos a aplicar sobre destino-viaje.component.html
  */
  constructor( private elRef: ElementRef) {
    this.elementRef = elRef.nativeElement;
    fromEvent(this.elementRef, 'click').subscribe( evento => this.track(evento));
  }
  track(evento: Event): void {
    // throw new Error('Method not implemented.');
    const elementTags = this.elementRef.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    console.log(`----> track evento: "${elementTags}"`);
  }

}
