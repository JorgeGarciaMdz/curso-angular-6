export interface IDestinoViajeModel {
  id?: number;
  nombre?: string;
  imagenUrl?: string;
  servicios?: string[];
  selected?: boolean;
  votes?: number;
  // setSelected(selected: boolean): void;
}

export class DestinoViajeModel implements IDestinoViajeModel{
  constructor(
    public id?: number,
    public nombre?: string,
    public imagenUrl?: string,
    public servicios?: string[],
    // tslint:disable-next-line:variable-name
    public selected: boolean = false,
    public votes: number = 0
    ){
      this.servicios = ['Desayuno', 'Almuerzo', 'Cena'];
      console.log('Creando Objeto DestinoViajeModel constructor con data: ' + JSON.stringify(this));
    }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(value: boolean): void {
    console.log('in setSelected de model ' + value);
    try {
      this.selected = value;
    } catch (err){
      console.log('---> error in setSelected, err: ' + err.message + ' --> ' + err);
    }
  }

  voteUp(): void {
    try {
      this.votes++;
    } catch (err){
      console.log('---> Error! no se puede sumar el voto (voteUp()): ' + err.message);
    }
  }

  voteDown(): void {
    try {
      this.votes--;
    } catch (err) {
      console.log('---> Error! no se puede quitar el voto in voteDown: ' + err.message);
    }
  }

  toString(): string {
    return 'nombre: ' + this.nombre + ', imagenUrl: ' + this.imagenUrl + ', selected: ' + this.selected
            + ', votes: ' + this.votes;
  }
}
