import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { APP_CONFIG, db, IAppConfig, IAppState } from '../app.module';
import { DestinoViajeModel, IDestinoViajeModel } from './destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';

interface IDestinosApiClient {
  add( destino: DestinoViajeModel): void;
  elegir( destino: DestinoViajeModel): void;
  getById(id: string): DestinoViajeModel;
}

@Injectable()
export class DestinosApiClient implements IDestinosApiClient{
  constructor(private store: Store<IAppState>) {
    console.log('---> in constructor DestinosApiClient');
  }

  add(destino: DestinoViajeModel): void {
    this.store.dispatch( new NuevoDestinoAction(destino));
  }

  elegir(destino: DestinoViajeModel): void {
    this.store.dispatch(new ElegidoFavoritoAction(destino));
  }

  getById(id: string): DestinoViajeModel {
    console.log('----> getById por DestinoApiClient');
    return null;
  }
}

/* Redefinos la clase
export class DestinosApiClient {

  destinoViajeModelArray?: DestinoViajeModel[];
  current: Subject<DestinoViajeModel> = new BehaviorSubject<DestinoViajeModel>(null); // se inicializa con null

  constructor(){
    this.destinoViajeModelArray = [];
  }

  add(destinoViajeModel: DestinoViajeModel): void {
    this.destinoViajeModelArray.push(destinoViajeModel);
  }

  getAll(): DestinoViajeModel[] {
    return this.destinoViajeModelArray;
  }

  getById(id: string ): DestinoViajeModel {
    // el filter devuelve un objeto Array, por eso se le indica el objeto en la posicion [0]
    return this.destinoViajeModelArray.filter( (destino: DestinoViajeModel) => {
      if ( destino.id.toString() === id) {
        return destino;
      } else {
        return null;
      }
    })[0];
  }

  elegir(destino: DestinoViajeModel): void {
    this.destinoViajeModelArray.forEach((dest: DestinoViajeModel ) => {
      // dest.setSelected(true);
      console.log('current is type Subject new BehaviorSuject, emit now object DestinoViajeModel '
                  + JSON.stringify(destino));
      dest.setSelected(true);
      this.current.next(dest); // emitiendo el evento
    });
  }

  suscribeCurrentOnChange(fn): void{ // fn is function
    console.log('suscribiendo al evento de current');
    this.current.subscribe(fn);
  }
} */

@Injectable()
export class DestinosApiClientV0 implements IDestinosApiClient{
  destinos: DestinoViajeModel[];

  constructor( private store: Store<IAppState> ) {
    console.log('---> in constructor DestinosApiCientV0');
    this.store
      .select(state => state.destinos)
      .subscribe( data => {
        console.log('---> Destinos subStore in destinosApiClientV0');
        // 1-  tratando de agregar id
        const i = data.items.length;
        data.items[ i - 1 ].id = i;
        // 1- no funca
        this.destinos = data.items;
      });
    this.store
      .subscribe( data => {
        console.log('---> all Store in destinosApiClientV0');
        console.log('data: ' + JSON.stringify(data));
      });
   }

  add(destino: DestinoViajeModel): void {
    const i: number = this.destinos.length;
    destino.id = i;
    this.destinos.push(destino);
  }
  elegir(destino: DestinoViajeModel): void {
    console.log('elegir dentro de DestinosApiClientV0');
    throw new Error('Method not implemented.');
  }

  getById(id: string): DestinoViajeModel {
    console.log('----> getById por DestinoApiClientV0');
    return this.destinos.filter( (data) => {
      console.log('buscando data by id: ' + JSON.stringify(data));
      return data.id.toString() === id;
    })[0];
  }
}

// V1 realiza las peticiones al backend
@Injectable()
export class DestinosApiClientV1 implements IDestinosApiClient{
  destinos: DestinoViajeModel[];

  // para conectarse a la api
  constructor( private store: Store<IAppState>,
               @Inject(forwardRef( () => APP_CONFIG)) private config: IAppConfig,
               private http: HttpClient) {
    console.log('---> in constructor DestinosApiCientV1');
    this.store
      .select( state => state.destinos)
      .subscribe( data => {
        console.log('--> IN Constructor DestinosApiClientV1, destinos subStore');
        console.log('-> Data: ' + JSON.stringify(data));
        this.destinos = data.items;
      });

    this.store
      .subscribe( data => {
        console.log('--> In Constructor DestinosApiClientV1, allStore');
        console.log('-> Data: ' + JSON.stringify(data));
      });
  }

  add( destino: DestinoViajeModel): void{
    console.log('---> in method add of DestinosApiClientV1');
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('POST', this.config.apiEndPoint + '/ciudades/create', { destino}, { headers });
    this.http.request(req).subscribe( ( data: HttpResponse<{}> ) => {
      if (data.status === 200) {
        this.store.dispatch( new NuevoDestinoAction( destino ));
        const myDb = db;
        console.log('---> agregando objeto a dexie con add, destino: ' + destino);
        myDb.destinos.add( destino );
        console.log('--> todos los destinos de la db ');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
  }

  elegir(destino: DestinoViajeModel): void {
    this.store.dispatch(new ElegidoFavoritoAction(destino));
  }

  getById(id: string): DestinoViajeModel {
    console.log('----> getById por DestinoApiClientV1');
    return this.destinos.filter( destino => {
      return destino.id.toString() === id;
    })[0];
    return null;
  }

  getAll(): DestinoViajeModel[] {
    return this.destinos;
  }
}
