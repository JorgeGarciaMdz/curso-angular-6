import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViajeModel } from './destino-viaje.model';
import { Injectable } from '@angular/core';

// Estado
export interface DestinosViajesState{
  items: DestinoViajeModel[];
  loading: boolean;
  favorito: DestinoViajeModel;
}

// da error el linter
// export const initializeDestinosViajeState = function(): { items: [], loading: boolean, favorito: null} {
/* otra forma de escribirlo es como una funcion exportada
export const initializeDestinosViajeState = (): { items: [], loading: boolean, favorito: null} => {
  console.log('inicializando reducer con initializeDestinosViajesState');
  return {
    items: [],
    loading: false,
    favorito: null
  };
}; */
export function initializeDestinosViajesState(): DestinosViajesState {
  return {
    items: [],
    loading: false,
    favorito: null
  };
}

// Acciones
export enum DestinosViajesActionType {
  NUEVO_DESTINO= '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] vote up',
  VOTE_DOWN = '[Destinos Viajes] vote down',
  // nuevo accion para la llamada a la api
  INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionType.VOTE_UP;
  constructor( public destino: DestinoViajeModel){
    console.log('Creando objeto: ' + destino.toString() + ' dentro de class VoteUpAction constructor');
  }
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionType.VOTE_DOWN;
  constructor( public destino: DestinoViajeModel) {
    console.log('Creando objeto: ' + destino.toString() + ' dentro de Class VoteDownAction constructor');
  }
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionType.NUEVO_DESTINO;
  constructor(public destino: DestinoViajeModel) {
    console.log('creando un NuevoDestionAction con ' + JSON.stringify(destino));
  }
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionType.ELEGIDO_FAVORITO;
  constructor( public destino: DestinoViajeModel) { }
}

export class InitMyDataAction implements Action {
  type = DestinosViajesActionType.INIT_MY_DATA;
  constructor( public destinos: string []) { }
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction  | InitMyDataAction;

// REDUCER
export function reducerDestinosViajes( state: DestinosViajesState,
                                       action: DestinosViajesActions): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionType.NUEVO_DESTINO: {
      console.log('in reducer new destino');
      return {
        ...state, // mantiene los datos de state y agrega al final un nuevo destino
        items: [ ...state.items, (action as  NuevoDestinoAction).destino]
      };
    }
    case DestinosViajesActionType.ELEGIDO_FAVORITO: {
      console.log('in reducer select favorito');
      state.items.forEach(x => {
        console.log('in state.items.forEach' + JSON.stringify(x) + x.isSelected());
        x.setSelected(false);
        console.log('despues de setSelected(false)');
      });
      const fav: DestinoViajeModel = (action as ElegidoFavoritoAction).destino;
      console.log(JSON.stringify(fav));
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav
      };
    }
    case DestinosViajesActionType.VOTE_UP: {
      console.log('in reducer case vote up');
      // el d ya esta dentro del estado
      const d: DestinoViajeModel = (action as VoteUpAction).destino;
      d.voteUp();
      return { ...state }; // retornamos el state clonado, el mismo
    }
    case DestinosViajesActionType.VOTE_DOWN: {
      console.log('in reducer case vote down');
      const d: DestinoViajeModel = (action as VoteDownAction).destino;
      d.voteDown();
      return { ...state };
    }
    case DestinosViajesActionType.INIT_MY_DATA: {
      console.log('in reducer case INIT_MY_DATA');
      const destinos: string [] = ( action as InitMyDataAction).destinos; // as actua como conversion de tipos
      return {
        ...state,
        items: destinos.map( destino => new DestinoViajeModel( null , destino, ''))
      };
    }
    default: {
      console.log('--> no entro en ningun state');
    }
  }
  return state;
}

// EFFECTS
// los effects son llamados luego de que se ejecutan todos los reducers
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionType.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor( private actions$: Actions) { }
}
