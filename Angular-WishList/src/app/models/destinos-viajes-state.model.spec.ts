import {
  reducerDestinosViajes,
  DestinosViajesState,
  initializeDestinosViajesState,
  InitMyDataAction,
  NuevoDestinoAction
} from './destinos-viajes-state.model';
import { DestinoViajeModel } from './destino-viaje.model';
import { State } from '@ngrx/store';

describe('reducerDestinosViajes', () => {
  it('El reducer deberia iniciar los datos', () => {
    // setup
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    // action
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    // assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });

  it('el recuder deberia agregar un nuevo item', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViajeModel(1, 'mendoza', 'url_mendoza'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('mendoza');
  });
});
