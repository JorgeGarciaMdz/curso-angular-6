export interface IDestino{
  id?: number;
  nombre?: string;
  url?: string;
}

export class Destino implements IDestino{
  constructor(
    public id?: number,
    public nombre?: string,
    public url?: string
  ){}
}
