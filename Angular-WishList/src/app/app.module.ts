import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// integracion para seguimiento en navegador web
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinoComponent } from './components/lista-destino/lista-destino.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
// se injecta de forma local en el componente que lo necesite
// import { DestinosApiClient } from './models/destinos-api-client.model';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesEffects, DestinosViajesState,   initializeDestinosViajesState, InitMyDataAction, reducerDestinosViajes } from './models/destinos-viajes-state.model';
import { ActionReducerMap, Store, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';

// importamos http para realizar peticiones, luego lo agregamos en imports
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';

// importando dexie
import Dexie from 'dexie';
import { DestinoViajeModel, IDestinoViajeModel } from './models/destino-viaje.model';

// importando translate
import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { flatMap } from 'rxjs/operators';
import { from, Observable } from 'rxjs';

// agregando Modulo para animaciones
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './directivas/espiame.directive';
import { TrackearClickDirective } from './directivas/trackear-click.directive';

// falta implementacion de MapBox, hay un error en la compilacion del componente

// init app config
export interface IAppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: IAppConfig = { apiEndPoint: 'http://localhost:3000'};

export const APP_CONFIG = new InjectionToken<IAppConfig>('app.config');
// fin init app config -> luego hay que proveerlo en prividers

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent }
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinoComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent},
  // ruta para el login
  { path: 'login', component: LoginComponent},
  { path: 'protected', component: ProtectedComponent, canActivate: [ UsuarioLogueadoGuard ] },
  // ruta para el modulo Vuelos
  { path: 'vuelos', component: VuelosComponent, canActivate: [ UsuarioLogueadoGuard], children: childrenRoutesVuelos }
];

// redux init
export interface IAppState {// se define el estado global de la aplicacion
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<IAppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};
// redux fin init

// app init para realizar las peticiones al backend
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor( private store: Store<IAppState>, private http: HttpClient) { }
  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/ciudades', { headers});
    const response: any = await this.http.request(req).toPromise(); // se puede hacer con el subscribe
    this.store.dispatch(new InitMyDataAction(response.body)); // y poner sta linea dentro de subscribe
  }
}
// fin app init para realizar las peticiones al backend

// inicio funcion translate
export class Translation {
  constructor( public id: number, public lang: string, public key: string, public value: string) { }
}
// fin funcion translate

// inicio dexie
@Injectable({ providedIn: 'root'})
export class MyDataBase extends Dexie {
  destinos: Dexie.Table<IDestinoViajeModel, number>;
  // agregando tabla para translate
  translations: Dexie.Table<Translation, number>;
  constructor( ) {
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl, servicios, selected, votes'
    });

    // versionado de Db
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl, servicios, selected, votes',
      translations: '++id, lang, key, value'
    });
    // this.destinos.mapToClass(DestinoViajeModel);
  }
}

export const db = new MyDataBase();
// fin inicio dexie

// init i18n
export class TranslationLoader implements TranslateLoader {
  constructor( private http: HttpClient) { }

  getTranslation( lang: string): Observable<any> {
    const promise = db.translations
      .where('lang')
      .equals(lang)
      .toArray()
      .then( results => {
        if ( results.length === 0 ){
          return this.http
            .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang )
            .toPromise()
            .then( apiResults => {
              db.translations.bulkAdd(apiResults);
              console.log('apiResult of translation: ' + JSON.stringify(apiResults));
              return apiResults;
            });
        }
        return results;
      }).then( (traducciones) => {
        console.log(' --> traducciones cargadas');
        console.log('--> value: ' + JSON.stringify(traducciones));
        return traducciones;
      }).then( (traducciones) => {
        return traducciones.map( (t) => ({ [t.key]: t.value}));
      });
    return from(promise).pipe(flatMap( (elems) => from(elems)));
  }
}

function HttpLoaderFactory( http: HttpClient): TranslateLoader {
  return new TranslationLoader(http);
}
// fin init i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule
    RouterModule.forRoot(routes),
    // para los formularios
    FormsModule,
    ReactiveFormsModule,
    // redux
    EffectsModule.forRoot([DestinosViajesEffects]),
    NgRxStoreModule.forRoot(reducers,
      { initialState: reducersInitialState,
        runtimeChecks: {  // permite que los estados muten o modifiquen una propiedad o atributo
          strictStateImmutability: false,
          strictActionImmutability: false,
        } }
      ),
    StoreDevtoolsModule.instrument({
      maxAge: 15 // retains last 25 state
    }),
    ReservasModule,
    // importamos el HttpClientModule
    HttpClientModule,
    // importando modulo para las traducciones
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    // importamos para que sea inyectable desde cualquier parte del proyecto
    // modulo de prueba, destino-viaje-component
    BrowserAnimationsModule
  ],
  providers: [
    // se injecta de forma local en el componente que lo necesite y no de forma global para todos los componentes
    // DestinosApiClient,
    AuthService,
    UsuarioLogueadoGuard,
    // provee la app config para inyectarlo en form-destino-viaje
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    // agregamos la inyeccion de dependencia
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDataBase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
