import { Component, EventEmitter, HostBinding, OnInit, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../app.module';
import { DestinoViajeModel} from '../../models/destino-viaje.model';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
// importando modulo para animiciones
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  // creamos la animacion en el decorador
  animations: [ // luego se agrega en el template
    trigger('esFavorito', [
      state('estadoFavorito', style({ backgroundColor: 'PaleTurquoise'})),
      state('estadoNoFavorito', style({ backgroundColor: 'WhitSmoke' })),
      transition('estadoNoFavorito => estadoFavorito', [ animate('3s') ]),
      transition('estadoFavorito => estadoNoFavorito', [ animate('1s')])
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  // destino: DestinoViajeModel;
  // @Input() nombre: string;
  @Input() destino: DestinoViajeModel;
  // tslint:disable-next-line:no-input-rename
  @Input('indx') position: number; // se renombra la entrada y en el template debe ser [indx] y no [position]
  // @Input() position: number; // viene del template lista-destino.component.html
  @HostBinding('attr.class') cssClass = 'col-md-4'; // envuelve el template url con el atributo col-md-4
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClicked: EventEmitter<DestinoViajeModel>; // se declara la variable clicked

  constructor( private store: Store<IAppState>) {
    this.onClicked = new EventEmitter();
    // this.destino = new DestinoViajeModel(null, 'un destino', 'uns dest');
   }

  ngOnInit(): void {
  }

  ir(): boolean {
    // tslint:disable-next-line:max-line-length
    this.onClicked.emit(this.destino); // clicked dispara el evento de emitir y da a conocer al componente padre
    return false;
  }

  voteUp(): boolean {
    this.store.dispatch( new VoteUpAction(this.destino));
    return false;
  }

  voteDown(): boolean {
    this.store.dispatch( new VoteDownAction(this.destino));
    return false;
  }

}
