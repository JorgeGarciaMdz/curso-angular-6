import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { DestinoViajeModel } from '../../models/destino-viaje.model';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, IAppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onItemAdded: EventEmitter<DestinoViajeModel>;
  minLongitudNombre = 3;
  searchResult: string[];

  fg: FormGroup; // la variable que se encuentra en le template html

  // injectando la appconfig de app.module en el constructor
  // tslint:disable-next-line:max-line-length
  constructor( fg: FormBuilder, @Inject(forwardRef( () => APP_CONFIG )) private config: IAppConfig) { // a traves de la firma del constructor se realiza la inyeccion de dependencia
    this.onItemAdded = new EventEmitter();
    this.fg = fg.group({ // group tiene un objeto de inicializacion que son los controls -> fg.controls['nombre']
      // nombre: ['', Validators.required],
      nombre: ['', Validators.compose([ // se compone todo lo que se quiere usar para validar
        Validators.required,
        // this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitudNombre)
      ])],
      url: ['', Validators.required]
    });

    // inicio agregado con llamada ajax
    this.fg.valueChanges.subscribe(
      (form: any) => {
        console.log('nombre cambio: ', form);
      }
    );

    this.fg.controls.nombre.valueChanges.subscribe(
      (value: string) => {
        console.log('nombre cambio ' + value);
      }
    );
    // fin inicio agregado con llamada ajax

    /* Esta propiedad esta atenta a cada entrada que se realiza al formulario tipo onChange de javascript
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario ' + JSON.stringify(form) );
    });
    */
  }

  ngOnInit(): void {
    // let elementoNombre = <HTMLInputElement>document.getElementById('nombre');
    const elementoNombre = document.getElementById('nombre') as HTMLInputElement;
    // escuchamos el evento de un tipeo en el input
     /* lo reemplazamos por la IAppConfig
      fromEvent(elementoNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), // value se asigna a text
        filter( text => text.length > 2), // filter evalua la condicion para pasar a la proxima cadena
        debounceTime(200), // pone en espera la lectura de teclado en 2s
        distinctUntilChanged(),
        switchMap(() => ajax('/assets/datos.json')) // simula la conexion a un servidor
      ).subscribe(ajaxResponse => {
        console.log(ajaxResponse.response);
        this.searchResult = ajaxResponse.response;
      }); */
    fromEvent(elementoNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), // value se asigna a text
        filter( text => text.length > 2), // filter evalua la condicion para pasar a la proxima cadena
        debounceTime(1000), // pone en espera la lectura de teclado en 2s
        distinctUntilChanged(),
        switchMap(( text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResult = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    const destino = new DestinoViajeModel( null, nombre, url);
    console.log('--> Guardar form-destino-viaje-component.ts con destino: ' + JSON.stringify(destino));
    this.onItemAdded.emit(destino);
    return false;
  }

  // validadores personalizados o customised
  nombreValidator(control: FormControl): { [message: string]: boolean }{
    const longNombre = control.value.toString().trim().length;
    if ( longNombre > 0 && longNombre < 5 ){
      return { invalidNombre: true };
    }
    return { invalidNombre: false};
  }

  // validador parametrizable
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [message: string]: boolean } | null => {
      const longNombre = control.value.toString().trim().length;
      if (longNombre > 0 && longNombre < this.minLongitudNombre ){
        return { minLongNombre: true };
      }
      return null;
    };
  }

}
