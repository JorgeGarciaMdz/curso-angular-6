import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/app.module';
import { DestinoViajeModel } from 'src/app/models/destino-viaje.model';
import { DestinosApiClient, DestinosApiClientV0, DestinosApiClientV1 } from 'src/app/models/destinos-api-client.model';

/* una api provee los datos
// inicio muestra otro modo de inyeccion de dependencia
interface AppConfig {
  apiEndPoint: string; // simulando url de enpoint
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi-api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorate extends DestinosApiClientV0 {
  // @Inject esta inyectando un valor y no un new de algo
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<IAppState>) {
    super(store);
  }

  // redefinimos el método getById
  getById(id: string): DestinoViajeModel {
    console.log('Llamado desde la clase DestinosApiClientDecorate ');
    console.log('endpoint config: ' + this.config.apiEndPoint);
    return super.getById(id);
  }
}
// fin muestra otro modo inyeccion de dependencia
*/

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  // forma 1
  /* providers: [
    DestinosApiClient,
    { provide: DestinosApiClientV0, useExisting: DestinosApiClient}
  ] */
  // fin forma 1
  // forma 2 una api provee los datos
  /* providers: [
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    // useClass la clase debe ser extension de la anterior
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorate },
    { provide: DestinosApiClientV0, useExisting: DestinosApiClient }
  ] */
  // fin forma 2

  /*  el providers provee solo una injeccion de dependencia, pero recibe el parametro de provide que se
    interpreta: utilizamos DestinosApiClientV0 pero si existe (useExisting) DestinosApiClient utilizamos
    este, cabe aclarar que los destinos deben impementar una interfaz para respetar la estructura
    de sus funciones
   */
  // providers: [DestinosApiClient]
  providers: [DestinosApiClientV1]
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViajeModel;
  public id?: number;

  // implementa la forma 1 como la forma 2
  constructor( private route: ActivatedRoute, private destinosApiClient: DestinosApiClientV1) { }

  ngOnInit(): void {
    // obtiene el parametro id de la url
    const id =  this.route.snapshot.paramMap.get('id');
    this.id = Number.parseInt(id, 10);
    this.destino = this.destinosApiClient.getById(id);
    console.log('Parametro recibido de url id: ' + this.id );
    // se debe correjir
    // this.destino = this.destinosApiClient.getById(id);
    // console.log('destino -> ' + this.destino);
  }

}
