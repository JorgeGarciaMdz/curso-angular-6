import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../app.module';
import { DestinoViajeModel } from '../../models/destino-viaje.model';
import { DestinosApiClient, DestinosApiClientV1 } from '../../models/destinos-api-client.model'; // contenedor de objetos
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  // importando de forma local la inyeccion de dependencia y no de forma global en app.module
  // providers: [DestinosApiClient]
  providers: [DestinosApiClientV1] // proveemos el V1 para llamada al backend
})
export class ListaDestinoComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onItemAdded: EventEmitter<DestinoViajeModel>;
  updates: string[];
  // se refactoriza api-client para que se use con redux, se crea una variable
  all;

  constructor( public destinosApiClient: DestinosApiClientV1,
               private store: Store<IAppState>) { // inyecta DestinosApiClient en destinosApiClient
    // this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    // se define la suscripcion pero falta disparar la accion
    // store tiene destinos: DestinosViajesState[items,loading, favorito]
    this.store.select(state => state.destinos.favorito) // nos interesa actualizaciones sobre favorito
                .subscribe(data => { // nos suscribimos a los cambios de favorito
                  if ( data != null ){ // a data llega lo que ha cambiado sobre favorito
                    console.log('dentro de la suscripcion de state');
                    this.updates.push('Se ha elegido "' + data.nombre + '" store');
                  }
                });

  /*  this.destinosApiClient.suscribeCurrentOnChange((d: DestinoViajeModel ) => {
      if ( d != null ){
        console.log('Inicializando la suscripcion a current de destinos-api-client.model');
        console.log('Se muestra en el template lista-destino-component');
        this.updates.push('Se ha elegino a ' + d.nombre);
      }
    }); */
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  /*guardar(nombre: string, url: string): boolean{
    console.log(`nombre: ${nombre}, url: ${url}`);
    this.destinos.push(new DestinoViajeModel(null, nombre, url));
    console.log(this.destinos);
    return false; // true -> recarga pagina; false -> no recarga pagina
  } */
  agregado(destinoViajeModel: DestinoViajeModel): void {
    this.destinosApiClient.add(destinoViajeModel);
    console.log('agregado en lista-destino-component.ts,  el model: ' + JSON.stringify(destinoViajeModel) + ' que viene del template listaDestino capturado por $event' );
    this.onItemAdded.emit(destinoViajeModel);
    // tambien disparamos la accion de la suscripcion de store
    // this.store.dispatch( new NuevoDestinoAction(destinoViajeModel)); se encuentr en el api-client
  }

  elegido(d: DestinoViajeModel): void {
    /* this.destinos.forEach((destino) => {
      destino.setSelected(false);
    }); */
    console.log('dentro de elegido');
    /* se reemplaza por el evento suscripto a current de destinos-api-client.model
    this.destinosApiClient.getAll().forEach( destino => {
      destino.setSelected(false);
    });
    d.setSelected(true); */
    this.destinosApiClient.elegir(d);
    // disparamos la acccion de la suscripcion de store
    // this.store.dispatch( new ElegidoFavoritoAction(d)); ya se encuentra en el api-client
  }

  // no hace falta debido que el template esta observando la variable all
  getAll(): DestinoViajeModel[] {
    return this.all;
  }

}
