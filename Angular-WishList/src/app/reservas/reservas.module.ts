// creado con ng g m reservas/reservas --module app --flat --routing
// crea un modulo app --flat se integra a app; --ruting usa un ruteo específico para dicho modulo

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasApiClientService } from './reservas-api-client.service';


@NgModule({
  declarations: [ReservasListadoComponent, ReservasDetalleComponent],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  // Provider Service
  providers: [ // quienes vas a proveer un SERVICIO
    ReservasApiClientService
  ]
})
export class ReservasModule { }
