// generado: ng g c reservas/reservas-listado; reservas se refiere al modulo, el generador lo conoce

import { Component, OnInit } from '@angular/core';
import { ReservasApiClientService} from '../reservas-api-client.service'; // 1. lo importamos

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {

  // 2. lo inyectamos
  constructor( public api: ReservasApiClientService) { }

  ngOnInit(): void {
  }

}
