import { Injectable } from '@angular/core';

// Injectabe => va a poder ser utilizado por otro componente
@Injectable({
  providedIn: 'root'
})
export class ReservasApiClientService {

  constructor() { }

  getAll(): IReservasApiClientModel[] {
    return [ { id: 1, name: 'nombre 1'},
             { id: 2, name: 'nombre 2' },
             { id: 3, name: 'nombre 3'}];
  }
}

// para respetar una estructura de datos
export interface IReservasApiClientModel {
  id?: any;
  name?: any;
}
