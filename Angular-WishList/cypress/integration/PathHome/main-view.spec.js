describe('ventana principal', () => {
  it('Tiene encabezado por defecto', () => {
    cy.visit('http://localhost:4200');
    cy.contains('angular-whishlist');
    // no funca los idiomas por el error 'map is not function' al mapear la respuesta del back-end
    cy.get('h1').should('contain', 'Angular-WishList');
  });
});